<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\InvoiceLines;


abstract class InvoiceLinesRepository implements RepositoryInterface
{
	public function find($id){
        return InvoiceLines::where('id',$id)->first();
    }
    public function findBy($where){
        return InvoiceLines::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return InvoiceLines::orderBy($orderBy,$direction)->get();    
        }
        return InvoiceLines::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=InvoiceLines::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	InvoiceLines::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=InvoiceLines::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=InvoiceLines::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>InvoiceLines::count()
    	];
    	return $response;
    }
}