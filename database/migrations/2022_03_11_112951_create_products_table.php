<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('brand_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('vat_id');
            $table->string('slug', 100)->unique();
            $table->boolean('active')->default(true);
            $table->string('title', 150);
            $table->text('description')->nullable(true);
            $table->string('thumbnail_path', 150)->nullable(true);
            $table->string('image_path', 150)->nullable(true);
            $table->float('price')->default(0);
            $table->unsignedSmallInteger('discount_percent')->default(0);
            $table->unsignedSmallInteger('units')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
