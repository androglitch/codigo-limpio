<?php

namespace App\Repositories\Interfaces;

interface OrderRepositoryInterface
{
    public function all($orderBy=null,$direction=null);

    public function store($id=null,array $data=null,$attachment=null);

    public function delete($id);

    public function find($id);

    public function findBy($where);

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false);
}