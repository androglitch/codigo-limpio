<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\User;


abstract class UserRepository implements RepositoryInterface
{
	public function find($id){
        return User::where('id',$id)->first();
    }
    public function findBy($where){
        return User::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return User::orderBy($orderBy,$direction)->get();    
        }
        return User::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=User::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	User::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=User::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=User::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>User::count()
    	];
    	return $response;
    }
}