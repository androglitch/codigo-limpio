<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoices extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'number',
        'client_id',
        'address_id',
        'base',
        'vat',
        'discount',
        'total',
    ];
}
