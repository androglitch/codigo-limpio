<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Categories;


abstract class CategoriesRepository implements RepositoryInterface
{
	public function find($id){
        return Categories::where('id',$id)->first();
    }
    public function findBy($where){
        return Categories::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return Categories::orderBy($orderBy,$direction)->get();    
        }
        return Categories::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=Categories::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	Categories::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=Categories::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=Categories::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>Categories::count()
    	];
    	return $response;
    }
}