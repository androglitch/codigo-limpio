<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Invoices;


abstract class InvoicesRepository implements RepositoryInterface
{
	public function find($id){
        return Invoices::where('id',$id)->first();
    }
    public function findBy($where){
        return Invoices::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return Invoices::orderBy($orderBy,$direction)->get();    
        }
        return Invoices::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=Invoices::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	Invoices::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=Invoices::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=Invoices::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>Invoices::count()
    	];
    	return $response;
    }
}