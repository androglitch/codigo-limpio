<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Products;


abstract class ProductsRepository implements RepositoryInterface
{
	public function find($id){
        return Products::where('id',$id)->first();
    }
    public function findBy($where){
        return Products::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return Products::orderBy($orderBy,$direction)->get();    
        }
        return Products::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=Products::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	Products::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=Products::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=Products::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>Products::count()
    	];
    	return $response;
    }
}