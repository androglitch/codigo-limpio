<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Carrier;


abstract class CarrierRepository implements RepositoryInterface
{
	public function find($id){
        return Carrier::where('id',$id)->first();
    }
    public function findBy($where){
        return Carrier::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return Carrier::orderBy($orderBy,$direction)->get();    
        }
        return Carrier::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=Carrier::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	Carrier::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=Carrier::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=Carrier::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>Carrier::count()
    	];
    	return $response;
    }
}