<?php

namespace App\Repositories\Implementations;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Brand;


abstract class BrandRepository implements RepositoryInterface
{
	public function find($id){
        return Brand::where('id',$id)->first();
    }
    public function findBy($where){
        return Brand::where($where)->first();
    }

    public function all($orderBy=null,$direction=null){
        if($orderBy!=null){
            $direction=$direction!=null?$direction:'asc';
            return Brand::orderBy($orderBy,$direction)->get();    
        }
        return Brand::get();
    }

    public function store($id=null,array $data=null,$attachment=null){
    	$newRegister=Brand::updateOrCreate(['id'=>$id],$data);
    	return $newRegister;
    }

    public function delete($id){
        $exists=$this->find($id)!=null;
    	Brand::where('id',$id)->delete();
        return $exists;
    }

    public function filter($filter,$orderBy,$limit,$page,$multiSort=false){
    	$where=[];
        foreach($filter as $key=>$value){
            if($value=='' || $value==null) continue;
            $where[]=[$key,'like','%'.$value.'%'];
        }
        $data=Brand::where($where)->skip($limit*($page-1))->limit($limit)->orderBy($orderBy['column'],$orderBy['direction']);
        $count=Brand::where($where);
    	$response=[
    		'data'=>$data->get(),
    		'count'=>$count->count(),
            'total'=>Brand::count()
    	];
    	return $response;
    }
}