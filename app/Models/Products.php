<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'brand_id',
        'category_id',
        'vat_id',
        'slug',
        'active',
        'title',
        'description',
        'thumbnail_path',
        'image_path',
        'price',
        'discount_percent',
        'units'
    ];
}
